const request = require('request');
const express = require('express')
const app = express()
app.use(express.json())

app.get('/', function (req, res) {
  res.redirect('https://gitlab.com/kekalainen/gitcord')
})

app.post('/api/webhooks/:webhook_id/:webhook_token/gitlab', function (req, res) {
  var body = req.body

  var requestJSON = {
    username: 'GitCord',
    avatar_url: 'https://gitlab.com/kekalainen/gitcord/raw/master/GitCord.png',
    embeds: [
    ]
  }

  var author = {
    name: body.user_name||body.user.name,
    icon_url: body.user_avatar||body.user.avatar_url,
  }

  var path_without_namespace = body.project.path_with_namespace.replace(body.project.namespace.toLowerCase()+'/','')

  if (body.object_kind === 'push') {
    body.commits.forEach(function(e) {
      var pushObj = {
        author: author,
        description: e.message,
        timestamp: new Date(e.timestamp).toISOString(),
        color: 16556838,
        footer: {
          text: path_without_namespace+':'+body.ref.replace('refs/heads/','')
        }
      }
      requestJSON.embeds.push(pushObj)
    });
  } else if (body.object_kind === 'tag_push') {
    var pushObj = {
      author: author,
      description: 'Pushed tag `'+body.ref.replace('refs/tags/','')+'`',
      timestamp: new Date().toISOString(),
      color: 14828329,
      footer: {
        text: path_without_namespace
      }
    }
    requestJSON.embeds.push(pushObj)
  } else if (body.object_kind === 'issue') {
    var issueActions = {
      open: 'Opened',
      close: 'Closed',
      reopen: 'Reopened',
      update: 'Updated'
    }
    var issueColor = 16477477
    if (body.labels[0] != null && body.labels[0].color != null) {
      issueColor = parseInt(body.labels[0].color.replace('#',''), 16);
    }
    var pushObj = {
      author: author,
      description: issueActions[body.object_attributes.action||'open']+' issue [#'+body.object_attributes.iid+': '+body.object_attributes.title+']('+body.object_attributes.url+')',
      timestamp: new Date().toISOString(),
      color: issueColor,
      footer: {
        text: path_without_namespace
      }
    }
    requestJSON.embeds.push(pushObj)
  } else if (body.object_kind === 'note') {
    if (body.object_attributes.noteable_type === 'Issue') {
      var issueColor = 16477477
      if (body.issue.labels[0] != null && body.issue.labels[0].color != null) {
        issueColor = parseInt(body.issue.labels[0].color.replace('#',''), 16);
      }
      var pushObj = {
        author: author,
        description: 'Commented on issue [#'+body.issue.iid+': '+body.issue.title+']('+body.object_attributes.url+')',
        timestamp: new Date().toISOString(),
        color: issueColor,
        footer: {
          text: path_without_namespace
        }
      }
      requestJSON.embeds.push(pushObj)
    }
  } else if (body.object_kind === 'pipeline' && body.object_attributes.finished_at != null) {
    var color
    switch (body.object_attributes.status) {
      case 'success':
        color = 1082696;
        break;
      case 'failed':
        color = 14494478;
        break;
      default:
        color = 4362204;
    }
    requestJSON.embeds.push({
      author: author,
      description: `Pipeline [#${body.object_attributes.id}](${`${body.project.web_url}/-/pipelines/${body.object_attributes.id}`}) ${body.object_attributes.detailed_status}`,
      timestamp: new Date().toISOString(),
      color: color,
      footer: {
        text: `${path_without_namespace}:${body.object_attributes.ref}`
      }
    })
  }

  if (requestJSON.embeds.length == 0) {
    return res.status(200).send()
  }

  request.post({
      url: `https://discordapp.com/api/webhooks/${req.params.webhook_id}/${req.params.webhook_token}`,
      json: requestJSON
    },
    function(err,httpResponse,body) {
      res.statusMessage = httpResponse.statusMessage
      res.status(httpResponse.statusCode).send()
    }
  )
})

app.listen(80)
console.log("App listening on port 80")
